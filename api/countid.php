<?php
defined('IN_CMS') or exit('No permission resources.');
/**
 * 点击统计
 */
$db = pc_base::load_model('hits_model');
if($input->get('modelid') && $input->get('id')) {
	$model_arr = array();
	$model_arr = getcache('model','commons');
	$modelid = intval($input->get('modelid'));
	$hitsid = 'c-'.$modelid.'-'.intval($input->get('id'));
	$r = $db->get_one(array('hitsid'=>$hitsid));
	if(!$r) exit;
	extract($r);
	echo "\$('#todaydowns".$input->get('id')."').html('$dayviews');";
	echo "\$('#weekdowns".$input->get('id')."').html('$weekviews');";
	echo "\$('#monthdowns".$input->get('id')."').html('$monthviews');";
} elseif($input->get('module') && $input->get('id')) {
	$module = $input->get('module');
	if((preg_match('/([^a-z0-9_\-]+)/i',$module))) exit('1');
	$hitsid = $module.'-'.intval($input->get('id'));
	$r = $db->get_one(array('hitsid'=>$hitsid));
	if(!$r) exit;
	extract($r);
}
?>
$('#hits<?php echo $input->get('id')?>').html('<?php echo $views?>');